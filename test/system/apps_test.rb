require "application_system_test_case"

class AppsTest < ApplicationSystemTestCase
  test "app" do
    begin_at = "2019/10/11 9:00:00"
    visit "/build"
    click_button begin_at

    name = "Foo bar"
    fill_in "input-name", with: name
    email = "foo@example.com"
    fill_in "input-email", with: email
    phone = "01234567890"
    fill_in "input-phone", with: phone
    click_button "登録"

    assert_text("以下の内容で申込が完了しました")
    assert_equal begin_at, find("#reservation-begin-at span").text
    assert_equal name, find("#reservation-name span").text
    assert_equal email, find("#reservation-email span").text
    assert_equal phone, find("#reservation-phone span").text
  end

end
