require 'test_helper'

class ReservationTest < ActiveSupport::TestCase
  test "success create" do
    assert Reservation.create(name: "foo", email: "foo@exapmle.com", phone: "0123456789",
                              begin_at: Time.local(2019, 1, 2, 12, 23, 00)).valid?
  end

  test "fail on wrong param type" do
    assert !Reservation.create(name: "foo", email: "foo@exapmle.com", phone: "0123456789",
                               begin_at: "abc").valid?
  end

  test "fail on entity non presence" do
    assert !Reservation.create(name: "hoge").valid?
  end

  test "fail on invalid email" do
    assert !Reservation.create(name: "foo", email: "foo@exapmle", phone: "0123456789",
                               begin_at: Time.local(2019, 1, 2, 12, 23, 00)).valid?
  end

  test "fail on invalid phone" do
    assert !Reservation.create(name: "foo", email: "foo@exapmle.com", phone: "110",
                               begin_at: Time.local(2019, 1, 2, 12, 23, 00)).valid?
    assert !Reservation.create(name: "foo", email: "foo@exapmle.com", phone: "0120123",
                               begin_at: Time.local(2019, 1, 2, 12, 23, 00)).valid?
  end

end
