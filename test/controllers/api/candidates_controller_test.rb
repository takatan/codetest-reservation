require 'test_helper'
require 'const'

module Api
  class CandidatesControllerTest < ActionDispatch::IntegrationTest
    test "should get index" do
      get api_candidates_url
      assert_response :success
      assert_equal CANDIDATES.as_json, JSON.parse(@response.body)
    end
  end
end