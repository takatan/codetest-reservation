require 'test_helper'
require 'const'
module Api
  class ReservationsControllerTest < ActionDispatch::IntegrationTest
    test "success post create" do
      count = Reservation.count
      name = "foo"
      email = "foo@example.org"
      phone = "0123456789"
      choice = 0
      post api_reservations_url, params: {name: name, email: email, phone: phone, choice: choice}
      assert_response :success
      response_json =JSON.parse(@response.body)
      assert_equal("SUCCESS", response_json["status"])
      assert_equal(name, response_json["data"]["name"])
      assert_equal(email, response_json["data"]["email"])
      assert_equal(phone, response_json["data"]["phone"])
      assert_equal(CANDIDATES[choice], response_json["data"]["begin_at"])

      assert_equal Reservation.count, count + 1
      last = Reservation.last
      assert_equal last.name, name
      assert_equal last.email, email
      assert_equal last.phone, phone
      assert_equal last.begin_at, CANDIDATES[choice]
    end

    test "fail on invalid email" do
      post api_reservations_url, params: {name: "foo", email: "foo@hoge", phone: "0123456789", choice: 0}
      assert_response :internal_server_error
      assert_equal "ERROR", JSON.parse(@response.body)['status']
    end

    test "fail on invalid name" do
      post api_reservations_url, params: {name: "", email: "foo@example.com", phone: "0123456789", choice: 0}
      assert_response :internal_server_error
      assert_equal "ERROR", JSON.parse(@response.body)['status']
    end

    test "fail on invalid phone" do
      post api_reservations_url, params: {name: "foo", email: "foo@example.com", phone: "119", choice: 0}
      assert_response :internal_server_error
      assert_equal "ERROR", JSON.parse(@response.body)['status']
    end

    test "fail on invalid choice" do
      post api_reservations_url, params: {name: "foo", email: "foo@example.com", phone: "0123456789", choice: 6}
      assert_response :internal_server_error
      assert_equal "ERROR", JSON.parse(@response.body)['status']
    end
  end
end