class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.string :name, :null => false, :index => true
      t.string :email, :null => false, :index => true
      t.string :phone, :null => false, :index => true
      t.datetime :begin_at, :null => false, :index => true

      t.timestamps
    end
  end
end
