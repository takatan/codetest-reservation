import React, { useEffect, useState } from "react";
import "./App.css";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  createMuiTheme,
  List,
  ListItem,
  ListItemText,
  MuiThemeProvider,
  Step,
  StepContent,
  StepLabel,
  Stepper,
  TextField
} from "@material-ui/core";
import { mapWithIndex } from "fp-ts/lib/Array";
import { fold, none, Option, some } from "fp-ts/lib/Option";
import { getCandidates, postReservation, Reservation } from "./api";
import * as E from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";

const theme = createMuiTheme({});

const VALID_EMAIL_REGEX = /^[\w+\-.]+@[a-z\d\-.]+\.[a-z]+$/i;
const VALID_PHONE_REGEX = /^0\d{9,10}$/i;
// const SAMPLE_CANDIDATES = [
//   new Date(2018, 1, 2, 12, 0, 0),
//   new Date(2018, 2, 2, 13, 0, 0),
//   new Date(2018, 3, 2, 14, 0, 0),
//   new Date(2018, 4, 2, 15, 0, 0),
//   new Date(2018, 5, 2, 16, 0, 0)
// ];

export const FormChooseCandidate: React.FC<{
  candidates: Date[];
  setPhase: (i: number) => void;
  setChoice: (c: Option<number>) => void;
}> = props => {
  const { setPhase, setChoice, candidates } = props;
  return (
    <List>
      {mapWithIndex((i: number, d: Date) => (
        <ListItem key={i}>
          <Button
            className="candidate"
            color="primary"
            variant="outlined"
            onClick={() => {
              setChoice(some(i));
              setPhase(1);
            }}
          >
            {d.toLocaleString()}
          </Button>
        </ListItem>
      ))(candidates)}
    </List>
  );
};

export const FormPersonalInfo: React.FC<{
  choice: number;
  candidate: string;
  setPhase: (s: number) => void;
  setReservation: (r: Reservation) => void;
}> = props => {
  const { choice, candidate, setPhase, setReservation } = props;

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");

  const nameValid = name.length > 0;
  const emailValid = VALID_EMAIL_REGEX.test(email);
  const phoneValid = VALID_PHONE_REGEX.test(phone);
  const disableNext = !(nameValid && emailValid && phoneValid);
  return (
    <div>
      <List>
        <ListItem>
          <ListItemText secondary="予約日時" primary={candidate} />
        </ListItem>
        <ListItem>
          <TextField
            id="input-name"
            required={true}
            label="名前"
            value={name}
            error={!nameValid}
            onChange={e => setName(e.target.value)}
          />
        </ListItem>
        <ListItem>
          <TextField
            id="input-email"
            required={true}
            label="Email"
            value={email}
            error={!emailValid}
            onChange={e => setEmail(e.target.value)}
          />
        </ListItem>
        <ListItem>
          <TextField
            id="input-phone"
            required={true}
            label="電話番号"
            helperText="ハイフン省略(例:090ABCD9876)"
            value={phone}
            error={!phoneValid}
            onChange={e => setPhone(e.target.value)}
          />
        </ListItem>
      </List>
      <Button variant="outlined" onClick={() => setPhase(0)}>
        日時選択に戻る
      </Button>
      <Button
        variant="contained"
        color="primary"
        disabled={disableNext}
        onClick={async () => {
          pipe(
            await postReservation({
              name,
              email,
              phone,
              choice
            }),
            E.fold(
              (e: Error) => {
                // TODO: change to better error notification
                alert(e.message);
              },
              (data: Reservation) => {
                setName("");
                setEmail("");
                setPhone("");
                setReservation(data);
                setPhase(2);
              }
            )
          );
        }}
      >
        登録
      </Button>
    </div>
  );
};

export const ReservationPrint: React.FC<{
  reservation: Reservation;
  setPhase: (i: number) => void;
  setChoice: (c: Option<number>) => void;
  setReservation: (r: Reservation) => void;
}> = props => {
  const { reservation, setChoice, setPhase, setReservation } = props;
  return (
    <Card>
      <CardHeader title="以下の内容で申込が完了しました" />
      <CardContent>
        <List>
          <ListItem>
            <ListItemText
              id="reservation-begin-at"
              secondary="予約日時"
              primary={new Date(reservation.begin_at).toLocaleString()}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              id="reservation-name"
              secondary="名前"
              primary={reservation.name}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              id="reservation-email"
              secondary="Email"
              primary={reservation.email}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              id="reservation-phone"
              secondary="電話番号"
              primary={reservation.phone}
            />
          </ListItem>
        </List>
        <Button
          variant="outlined"
          onClick={() => {
            setChoice(none);
            setPhase(0);
            setReservation({ name: "", email: "", phone: "", begin_at: "" });
          }}
        >
          最初に戻る
        </Button>
      </CardContent>
    </Card>
  );
};

export const App: React.FC = () => {
  const [phase, setPhase] = useState(0);
  const [choice, setChoice] = useState<Option<number>>(none);
  const [candidates, setCandidates] = useState<Date[]>([]);
  const [reservation, setReservation] = useState<Reservation>({
    name: "",
    email: "",
    phone: "",
    begin_at: ""
  });
  useEffect(() => {
    const f = async () => {
      pipe(
        await getCandidates(),
        E.fold(
          () => setCandidates([]),
          (ds: Date[]) => setCandidates(ds)
        )
      );
    };
    f();
  }, []);

  return (
    <MuiThemeProvider theme={theme}>
      <div className="App">
        <div className="App-body">
          <Stepper activeStep={phase} orientation="vertical">
            <Step>
              <StepLabel className="subtitle">予約日時を選択</StepLabel>
              <StepContent>
                <FormChooseCandidate
                  candidates={candidates}
                  setChoice={setChoice}
                  setPhase={setPhase}
                />
              </StepContent>
            </Step>
            <Step>
              <StepLabel>申込者情報の入力</StepLabel>
              <StepContent>
                {fold(
                  () => <span />,
                  (c: number) => (
                    <FormPersonalInfo
                      candidate={candidates[c].toLocaleString()}
                      choice={c}
                      setPhase={setPhase}
                      setReservation={setReservation}
                    />
                  )
                )(choice)}
              </StepContent>
            </Step>
          </Stepper>
          {phase === 2 && (
            <ReservationPrint
              reservation={reservation}
              setPhase={setPhase}
              setChoice={setChoice}
              setReservation={setReservation}
            />
          )}
        </div>
      </div>
    </MuiThemeProvider>
  );
};
