import axios, { AxiosResponse } from "axios";
import { array, mapWithIndex } from "fp-ts/lib/Array";
import {
  chain,
  either,
  Either,
  left,
  mapLeft,
  right,
  toError
} from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { tryCatch } from "fp-ts/lib/TaskEither";
import * as t from "io-ts";
import { flow } from "fp-ts/lib/function";

export const getCandidates = async (): Promise<Either<Error, Date[]>> => {
  const axiosTask = tryCatch(
    () => axios.get("/api/candidates"),
    e => (e instanceof Error ? e : toError("unknown error"))
  );
  return pipe(
    await axiosTask(),
    chain((res: AxiosResponse) => {
      if (res.status === 200) {
        const stringArray = t.array(t.string);
        const data = res.data;
        return pipe(
          stringArray.decode(data),
          mapLeft((_: t.Errors) => new Error(`invalid JSON: "${data}"`)),
          chain(
            flow(
              mapWithIndex<string, Either<Error, Date>>(
                (i: number, s: string) => {
                  const d = new Date(s);
                  return d.toString() === "Invalid Date"
                    ? left(new Error(`${i}: ${s}`))
                    : right(d);
                }
              ),
              array.sequence(either)
            )
          )
        );
      } else {
        return left(new Error(`status is ${res.status}`));
      }
    })
  );
};

export interface FormInput {
  name: string;
  email: string;
  phone: string;
  choice: number;
}
export interface Reservation {
  name: string;
  email: string;
  phone: string;
  begin_at: string;
}
export const postReservation = async (
  reservation: FormInput
): Promise<Either<Error, Reservation>> => {
  const axiosTask = tryCatch(
    () => axios.post("/api/reservations", reservation),
    e => (e instanceof Error ? e : toError("unknown error"))
  );
  return pipe(
    await axiosTask(),
    chain((res: AxiosResponse) => {
      if (res.status === 200) {
        const ResponseJson = t.union([
          t.type({
            status: t.literal("SUCCESS"),
            message: t.string,
            data: t.type({
              name: t.string,
              email: t.string,
              phone: t.string,
              begin_at: t.string
            })
          }),
          t.type({ status: t.literal("ERROR"), message: t.string })
        ]);
        const data = res.data;
        return pipe(
          ResponseJson.decode(data),
          mapLeft((_: t.Errors) => toError(`invalid JSON: "${data}"`)),
          chain(r => {
            if (r.status === "SUCCESS") {
              return right(r.data);
            } else {
              return left(toError(r.message));
            }
          })
        );
      } else {
        return left(
          toError(`status is ${res.status}. response is ${res.data}.`)
        );
      }
    })
  );
};
