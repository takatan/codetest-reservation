import { getCandidates, postReservation, FormInput } from "./api";
import { isLeft, isRight, left, right } from "fp-ts/lib/Either";
import moxios from "moxios";

describe("getCandidates", () => {
  it("get error without mock", async () => {
    const c = getCandidates();
    const res = await c;
    expect(isLeft(res)).toBeTruthy();
  });
  describe("with mock", () => {
    beforeEach(function() {
      moxios.install();
    });

    afterEach(function() {
      moxios.uninstall();
    });

    it("get dates with moxios", async () => {
      const jsonText =
        '["2019-10-11T09:00:00.000+09:00","2019-12-01T12:15:00.000+09:00"]';
      const dates = [
        new Date("2019-10-11T09:00:00.000+09:00"),
        new Date("2019-12-01T12:15:00.000+09:00")
      ];
      moxios.stubRequest("/api/candidates", {
        status: 200,
        responseText: jsonText
      });

      const c = getCandidates();
      const res = await c;
      expect(res).toStrictEqual(right(dates));
    });

    it("get error with wrong date format", async () => {
      const jsonText = '["2019-10-11T09:00:00.000+09:00","abcdef"]';
      moxios.stubRequest("/api/candidates", {
        status: 200,
        responseText: jsonText
      });

      const c = getCandidates();
      const res = await c;
      expect(isLeft(res)).toBeTruthy();
    });

    it("get error with wrong json", async () => {
      const jsonText = '"abcde"';
      moxios.stubRequest("/api/candidates", {
        status: 200,
        responseText: jsonText
      });

      const c = getCandidates();
      const res = await c;
      expect(isLeft(res)).toBeTruthy();
    });
  });
});
describe("postReservation", () => {
  const sample: FormInput = {
    name: "foo",
    email: "foo@example.com",
    phone: "0123456789",
    choice: 0
  };
  it("get error without mock", async () => {
    const res = await postReservation(sample);
    expect(isLeft(res)).toBeTruthy();
  });
  describe("with mock", () => {
    beforeEach(function() {
      moxios.install();
    });

    afterEach(function() {
      moxios.uninstall();
    });
    it("save success", async () => {
      moxios.stubRequest("/api/reservations", {
        status: 200,
        responseText:
          '{"status":"SUCCESS","message":"saved",' +
          '"data":{"id":8,"name":"hoge","email":"hoge@example.com",' +
          '"phone":"01234567890","begin_at":"2019-10-11T00:00:00.000Z",' +
          '"created_at":"2019-11-12T12:00:00.000Z","updated_at":"2019-11-14T15:00:00.000Z"}}'
      });
      const res = await postReservation(sample);
      expect(res).toStrictEqual(
        right({
          id: 8,
          name: "hoge",
          email: "hoge@example.com",
          phone: "01234567890",
          begin_at: "2019-10-11T00:00:00.000Z",
          created_at: "2019-11-12T12:00:00.000Z",
          updated_at: "2019-11-14T15:00:00.000Z"
        })
      );
    });
    it("save failed", async () => {
      moxios.stubRequest("/api/reservations", {
        status: 200,
        responseText: JSON.stringify({
          status: "ERROR",
          message: "failed to save"
        })
      });
      const res = await postReservation({ ...sample, name: "" });
      expect(res).toStrictEqual(left(new Error("failed to save")));
    });
  });
});
