import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

// Avoid Warning: useLayoutEffect does nothing on the server,
// because its effect cannot be encoded into the server renderer's output format...
import React from "react";

React.useLayoutEffect = React.useEffect;
