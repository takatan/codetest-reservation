import React from "react";
import { App, FormPersonalInfo } from "./App";
import { mount } from "enzyme";
import { act } from "react-dom/test-utils";
import moxios from "moxios";

// it("renders without crashing", () => {
//   const div = document.createElement("div");
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

describe("<App />", () => {
  describe("with mock", () => {
    beforeEach(function() {
      moxios.install();
    });

    afterEach(function() {
      moxios.uninstall();
    });

    it("mount", async () => {
      const jsonText =
        '["2019-10-11T09:00:00.000+09:00","2019-12-01T12:15:00.000+09:00"]';
      moxios.stubRequest("/api/candidates", {
        status: 200,
        responseText: jsonText
      });

      let wrapper = mount(<App />);
      await act(async () => {
        await wrapper;
        await new Promise(resolve => setImmediate(resolve));
        wrapper.update();
      });
      const button = wrapper.find("button.candidate");
      // button.forEach(x => console.log(x.debug()));
      expect(button.length).toBe(2);
    });
  });
});

describe("<FormPersonalInfo />", () => {
  it("button disabled before input", () => {
    const wrapper = mount(
      <FormPersonalInfo
        choice={0}
        candidate="xyz"
        setPhase={() => {}}
        setReservation={() => {}}
      />
    );
    const button = wrapper.find("button").at(1);
    expect(button.prop("disabled")).toBe(true);
  });
  it("button not disabled after input", () => {
    const wrapper = mount(
      <FormPersonalInfo
        choice={0}
        candidate="xyz"
        setPhase={() => {}}
        setReservation={() => {}}
      />
    );
    const input_name = wrapper.find("#input-name").first();
    const input_email = wrapper.find("#input-email").first();
    const input_phone = wrapper.find("#input-phone").first();
    input_name.simulate("change", { target: { value: "foo" } });
    input_email.simulate("change", { target: { value: "foo@example.com" } });
    input_phone.simulate("change", { target: { value: "01234567890" } });
    const button = wrapper.find("button").at(1);
    expect(button.prop("disabled")).toBe(true);
  });
});
