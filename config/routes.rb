Rails.application.routes.draw do
  namespace "api" do
    resources :candidates, only: [:index]
    resources :reservations, only: [:create]
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
