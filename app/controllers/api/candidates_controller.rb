require 'const'
module Api
  class CandidatesController < ApplicationController
    def index
      render json: CANDIDATES
    end
  end
end