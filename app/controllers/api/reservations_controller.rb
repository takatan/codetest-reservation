require 'const'

module Api
  class ReservationsController < ApplicationController
    def create
      reservation = Reservation.new(name: params[:name], email: params[:email], phone: params[:phone],
                                    begin_at: CANDIDATES[params[:choice].to_i])
      if reservation.save
        render json: {status: 'SUCCESS', message: "saved", data: reservation}
      else
        # reservation.errors
        render json: {status: 'ERROR', message: "failed to save"}, status: 500
      end
    end
  end
end