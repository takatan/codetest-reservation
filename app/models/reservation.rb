class Reservation < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_PHONE_REGEX = /\A0\d{9,10}\z/i

  validates :name, presence: true, length: {minimum: 1}
  validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}
  validates :phone, presence: true, format: {with: VALID_PHONE_REGEX}
  validates :begin_at, presence: true
end
